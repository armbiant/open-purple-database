-- CREATE DATABASE talentdb;
-- USE talentdb;

-- Step 1. Create the main tables

BEGIN TRANSACTION;

CREATE TABLE person (
    person_id SERIAL PRIMARY KEY,
    fullname VARCHAR(50),
    email VARCHAR(50),
    branch VARCHAR(50),
    year_of_studies VARCHAR(20),
    level_of_degree VARCHAR(50),
    last_mandate VARCHAR(50)
);

CREATE TABLE position (
    position_id SERIAL PRIMARY KEY,
    position_title VARCHAR(50),
    position_type VARCHAR(50),
    previous_title VARCHAR(50)
);

CREATE TYPE skills AS ENUM ('soft_skill', 'hard_skill', 'personality_trait');

CREATE TABLE skill (
    skill_id SERIAL PRIMARY KEY,
    skill_name VARCHAR(50),
    skill_type skills
);

CREATE TYPE identity AS ENUM ('Person', 'Position');

CREATE TABLE matching (
    match_id SERIAL,
    entry_id INT,
    skill_id INT,
    rating INT,
    entry_type identity,
    PRIMARY KEY (match_id),
    CONSTRAINT fk_skills
        FOREIGN KEY (skill_id) 
            REFERENCES skill(skill_id)
);

--COMMIT --ROLLBACK