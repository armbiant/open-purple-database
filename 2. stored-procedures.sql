-- Step 2. Create the Functions 

BEGIN TRANSACTION;

CREATE FUNCTION position_skills (our_position_title VARCHAR(50))
RETURNS TABLE (
	my_position VARCHAR,
	Skill VARCHAR,
	Rating INT,
	my_type skills
)
LANGUAGE plpgsql
AS $$

BEGIN

RETURN QUERY
	SELECT 
		position_title, 
        skill_name, 
        matching.rating,
        skill_type 
	FROM position
	INNER JOIN matching ON entry_id = position_id
	INNER JOIN skill 	ON matching.skill_id = skill.skill_id
	WHERE 	entry_type = 'Position'
		AND position_title = our_position_title
    ORDER BY skill_type, matching.rating DESC;

END; $$

CREATE FUNCTION personal_skills (our_email VARCHAR(50))
RETURNS TABLE (
	my_fullname VARCHAR,
	Skill VARCHAR,
	Rating INT,
	my_type skills
)
LANGUAGE plpgsql
AS $$

BEGIN

RETURN QUERY
	SELECT 
		fullname			AS Name, 
        skill_name			AS Skill, 
        matching.rating	 	AS Rating,
        skill_type			AS Type 
	FROM person
	INNER JOIN matching ON entry_id = person_id
	INNER JOIN skill 	ON matching.skill_id = skill.skill_id
	WHERE 	entry_type = 'Person'
		AND email = our_email
    ORDER BY skill_type, matching.rating DESC;
    
END; $$

CREATE FUNCTION worst_skills (our_skill_type skills)
RETURNS TABLE (
	Skill VARCHAR,
	AVGRating NUMERIC,
	my_type skills
)
LANGUAGE plpgsql
AS $$

BEGIN

RETURN QUERY
	SELECT 
		skill_name		AS Skill,
		ROUND( CAST(AVG(rating) as NUMERIC), 1) AS AverageRating,
        skill_type		AS SkillType		
	FROM matching
	INNER JOIN skill ON matching.skill_id = skill.skill_id
	WHERE 	entry_type = 'Person'
		AND skill_type = our_skill_type
    GROUP BY skill_name, skill_type
    ORDER BY skill_type, 2;
    
END; $$

CREATE FUNCTION position_matching(position_name VARCHAR(50))
RETURNS TABLE (
	Name VARCHAR,
    Email VARCHAR, 
    OverallScore NUMERIC
)
LANGUAGE plpgsql
AS $$

DECLARE per_id INTEGER;
DECLARE flag INTEGER;
    
DECLARE personCur CURSOR FOR
	SELECT person_id 
    FROM person;

BEGIN

    
    -- PART 1, getting the skills of the position --
	CREATE TEMPORARY TABLE tempPosSkills
    (
		skill_id INTEGER,
        rating INTEGER,
        skill_type VARCHAR(20),
        PRIMARY KEY(skill_id)
    );
    
    INSERT INTO tempPosSkills
		SELECT 
			skill.skill_id			AS Skill, 
			rating	 			AS Rating,
			skill_type			AS Type 
		FROM position
		INNER JOIN matching ON entry_id = position_id
		INNER JOIN skill 	ON matching.skill_id = skill.skill_id
		WHERE 	entry_type = 'Position'
			AND position_title = position_name
		ORDER BY skill_type, rating DESC;
    
	-- PART 2, THE FUN PART --
	
    CREATE TEMPORARY TABLE tempPerSkills
    (
		person_id INTEGER,
        sum_rating INTEGER, 
		PRIMARY KEY (person_id)
    );
    
    
    OPEN personCur;

	LOOP

    	FETCH personCur INTO per_id;
    	exit when not found;

		INSERT INTO tempPerSkills
			    SELECT person_id, SUM(ABS((matching.rating - tempPosSkills.rating) * tempPosSkills.rating))
				FROM person 
					INNER JOIN matching ON entry_id = person_id 
					INNER JOIN tempPosSkills ON matching.skill_id = tempPosSkills.skill_id 
				WHERE person_id = per_id AND entry_type = 'Person'
				GROUP BY person_id;
    END LOOP;

	CLOSE personCur;
    
RETURN QUERY
    SELECT 
		person.fullname	 	AS Name,
        person.email 			AS Email, 
        ROUND( CAST(sum_rating as NUMERIC), 2) 		AS OverallScore
    FROM person 
	INNER JOIN tempPerSkills ON person.person_id  = tempPerSkills.person_id
    ORDER BY 3;
    
    
    DROP TABLE tempPosSkills;
    DROP TABLE tempPerSkills;
    
END; $$

--COMMIT --ROLLBACK